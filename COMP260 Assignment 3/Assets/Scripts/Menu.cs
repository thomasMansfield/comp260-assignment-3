﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour {
    public GameObject shellPanel;

    private bool paused = false;

	// Use this for initialization
	void Start () {
        SetPaused(paused);
	}
	
	// Update is called once per frame
	void Update () {
        //pause if the player presses escape
        if (!paused && Input.GetKeyDown(KeyCode.Escape))
        {
            SetPaused(true);
        }
	}

    public void OnPressedPlay()
    {
        SetPaused(false);
    }

    public void OnPressedQuit()
    {
        Application.Quit();
    }

    private void SetPaused(bool p)
    {
        //make the shell panel (in)active when (un)paused
        paused = p;
        shellPanel.SetActive(paused);
        Time.timeScale = paused ? 0 : 1;
    }
}
