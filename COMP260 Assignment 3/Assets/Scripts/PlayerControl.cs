﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {

    public float maxSpeed = 10f;
    public float gravity = -1f;
    public float yMove;
    public AudioClip pointCollideClip;
    public AudioClip hazardCollideClip;
    public Rigidbody2D rb;
    public Transform respawn;

    private SpriteRenderer renderer;
    private AudioSource audio;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();
        renderer = GetComponent<SpriteRenderer>();
        audio = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {

        //reverse gravity when arrow key is pressed
        if ((Input.GetKeyDown(KeyCode.UpArrow) && gravity == -1)|| (Input.GetKeyDown(KeyCode.DownArrow) && gravity == 1))
        {
            gravity *= -1;
            renderer.flipY = !renderer.flipY;
        }

        //move in direction of gravity
        yMove = gravity;

        //set direction and move
        float direction = 0;
        if (Input.GetKey(KeyCode.LeftArrow)) {
            direction = -1;
        } else if (Input.GetKey(KeyCode.RightArrow)) {
            direction = 1;
        }

        //set sprite direction
        if ((direction > 0 && renderer.flipX) || (direction < 0 && !renderer.flipX))
        {
            renderer.flipX = !renderer.flipX;
        }

        Vector2 dir = new Vector2(direction, yMove);

        rb.velocity = dir.normalized * maxSpeed;
        
        //clamp velocity
        if (rb.velocity.magnitude >= maxSpeed)
        {
            rb.velocity = Vector2.ClampMagnitude(rb.velocity, maxSpeed);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Player collided with: " + collision.gameObject.name);

        if (collision.gameObject.tag == "Enemy")
        {
            audio.PlayOneShot(hazardCollideClip);
            Vector2 respawnPoint = respawn.position;
            rb.position = respawnPoint;
        }

        if (collision.gameObject.tag == "Point")
        {
            audio.PlayOneShot(pointCollideClip);
            //increment points counter by 1
            GameControl.Instance.ScoreIncrement();
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.tag == "Envir" && yMove != 0)
        {
            yMove = 0;
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Respawn")
            respawn = collider.gameObject.transform;
        else if (collider.gameObject.tag == "Finish")
            GameControl.Instance.Win();
    }

}
