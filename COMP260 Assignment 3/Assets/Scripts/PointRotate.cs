﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointRotate : MonoBehaviour {

    public float degPerFrame;

    private Transform transform;

    void Start()
    {
        transform = GetComponent<Transform>();
    }

	// Update is called once per frame
	void Update () {
        transform.Rotate(new Vector3(0, 0, degPerFrame));
	}
}
