﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackForthMove : MonoBehaviour {

    public float speed;
    public Transform transform;
    public Transform endPoint;

    private float dirSwitchDelay = 2f;
    private Vector2 startLoc;
    private Vector2 endLoc;

    void Start()
    {
        transform = GetComponent<Transform>();
        startLoc = transform.position;
        endLoc = endPoint.position;
    }
	
	// Update is called once per frame
	void Update () {
        
        if (dirSwitchDelay > 0)
        {
            dirSwitchDelay = dirSwitchDelay - Time.deltaTime;
        }

        //get target direction
        Vector2 pos = transform.position;
        Vector2 dir = endLoc - pos;
        dir = dir.normalized;
        
        //move to target
        Vector2 velocity = dir * speed;
        transform.Translate(velocity * Time.deltaTime);

        //check for reach end
        if (((endLoc != startLoc) && (dir.x <= 0 && dir.y <= 0) && dirSwitchDelay <= 0) || //case where going forward
            (endLoc == startLoc && (dir.x >= 0 && dir.y >= 0) && dirSwitchDelay <= 0)) //case where going backward
        {
            //switch endLoc
            if (endLoc != startLoc)
            {
                endLoc = startLoc;
            } else
            {
                endLoc = endPoint.position;
            }

            //reset timer
            dirSwitchDelay = 2f;
        }
    }

}
