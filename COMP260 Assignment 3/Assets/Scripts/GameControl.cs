﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControl : MonoBehaviour {

    public int points = 0;
    public Text pointsText;
    public Text winText;
    public Rigidbody2D player;

    static private GameControl instance;

	// Use this for initialization
	void Start () {
		if (instance == null)
        {
            //save this instance
            instance = this;
        }
        else
        {
            //more than one instance exists, error
            Debug.LogError("More than one GameControl");
        }
        pointsText.text = "Points: 0";
        winText.text = "";
	}

    public void ScoreIncrement()
    {
        points++;
        pointsText.text = "Points: " + points;
    }

    public void Win()
    {
        winText.text = "Huzzah! You Won!";
    }
	
    static public GameControl Instance
    {
        get { return instance; }
    }
}
